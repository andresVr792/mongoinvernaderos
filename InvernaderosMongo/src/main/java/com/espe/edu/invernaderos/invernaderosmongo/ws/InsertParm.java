/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.ws;

import com.espe.edu.invernaderos.invernaderosmongo.model.Step;

/**
 *
 * @author damia
 */
public class InsertParm {

    private Step step;
    private Integer catalogIdentity;
    private String cycleName;

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }

    public Integer getCatalogIdentity() {
        return catalogIdentity;
    }

    public void setCatalogIdentity(Integer catalogIdentity) {
        this.catalogIdentity = catalogIdentity;
    }

    public String getCycleName() {
        return cycleName;
    }

    public void setCycleName(String cycleName) {
        this.cycleName = cycleName;
    }

}
