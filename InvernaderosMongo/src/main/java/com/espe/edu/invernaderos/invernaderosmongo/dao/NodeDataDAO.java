/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.dao;

import com.espe.edu.invernaderos.invernaderosmongo.model.NodeData;
import com.espe.edu.invernaderos.invernaderosmongo.persistence.PersistenceManager;
import org.mongodb.morphia.dao.BasicDAO;
import org.bson.types.ObjectId;

/**
 *
 * @author Andres Vr
 */
public class NodeDataDAO extends BasicDAO<NodeData, ObjectId>{
    
    public NodeDataDAO() {
        super(NodeData.class,PersistenceManager.getMongoClient(),PersistenceManager.getMorphia(),"invernadero");
    }
    
}
