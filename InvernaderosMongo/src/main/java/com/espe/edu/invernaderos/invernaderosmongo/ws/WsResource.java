/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.ws;

import com.espe.edu.invernaderos.invernaderosmongo.model.Catalog;
import com.espe.edu.invernaderos.invernaderosmongo.model.Cycle;
import com.espe.edu.invernaderos.invernaderosmongo.model.NodeData;
import com.espe.edu.invernaderos.invernaderosmongo.model.Step;
import com.espe.edu.invernaderos.invernaderosmongo.service.CatalogService;
import com.espe.edu.invernaderos.invernaderosmongo.service.CycleService;
import com.espe.edu.invernaderos.invernaderosmongo.service.NodeDataService;
import com.espe.edu.invernaderos.invernaderosmongo.service.StepService;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Andres Vr
 */
@Path("ws")
public class WsResource {

    @EJB
    private NodeDataService nodeService;
    @EJB
    private CycleService cycleService;
    @EJB
    private StepService stepService;
    @EJB
    private CatalogService catalogService;
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of WsResource
     */
    public WsResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.espe.edu.invernaderos.invernaderosmongo.ws.WsResource
     *
     * @return an instance of java.lang.String
     */
    /**
     *
     * @param nodeData
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(NodeData nodeData) {

        this.nodeService.insert(nodeData);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<NodeData> findAll() {
        return this.nodeService.findAll();
    }

    @POST
    @Path("/cycle")
    @Consumes({MediaType.APPLICATION_JSON})
    public void createCycle(Cycle cycle) {

        this.cycleService.insert(cycle);
        System.out.println("Cycle inserted:" + cycle);
    }

    @POST
    @Path("/cycleUpdate")
    @Consumes({MediaType.APPLICATION_JSON})
    public void updateCycleState() {

        this.cycleService.updateState();
    }
    
    @POST
    @Path("/cycleUpdateGeneric")
    @Consumes({MediaType.APPLICATION_JSON})
    public void updateCycleStateGeneric(String name) {
        System.out.println("name: "+name);
        this.cycleService.updateStep(name);
    }

    @GET
    @Path("/cycle")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Cycle> findAllCycles() {
        return this.cycleService.getAll();
    }

    @GET
    @Path("/cycleLast")
    @Produces({MediaType.APPLICATION_JSON})
    public Cycle findLastCycle() {
        return this.cycleService.findLastCycle();
    }
//step

    @POST
    @Path("/step")
    @Consumes({MediaType.APPLICATION_JSON})
    public void createStep(InsertParm insertParm) {

        this.stepService.insert(insertParm);
    }

    @POST
    @Path("/stepUpdateStart")
    @Consumes({MediaType.APPLICATION_JSON})
    public void updateStartValueStep(StepService.UpdateStartValueDate updateStartValueDate) {

        this.stepService.updateStartValueDate(updateStartValueDate);
    }

    @POST
    @Path("/stepUpdateEnd")
    @Consumes({MediaType.APPLICATION_JSON})
    public void updateEndValueStep(StepService.UpdateEndValueDate updateEndValueDate) {

        this.stepService.updateEndValueDate(updateEndValueDate);
    }

    @GET
    @Path("/step")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Step> findAllSteps() {
        System.out.println("findAllSteps: " + this.stepService.findAll());
        return this.stepService.findAll();
    }

    @POST
    @Path("/catalog")
    @Consumes({MediaType.APPLICATION_JSON})
    public void insertCatalog(Catalog catalog) {
        System.out.println("insertCatalog: " + catalog);
        this.catalogService.insertCatalog(catalog);
    }

    @GET
    @Path("/catalog")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Catalog> getAllCatalogs() {
        System.out.println("getAllCatalogs: " + this.catalogService.getAll());
        return this.catalogService.getAll();
    }
}
