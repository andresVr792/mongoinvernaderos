/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.service;

import com.espe.edu.invernaderos.invernaderosmongo.dao.CatalogDAO;
import com.espe.edu.invernaderos.invernaderosmongo.dao.CycleDAO;
import com.espe.edu.invernaderos.invernaderosmongo.dao.StepDAO;
import com.espe.edu.invernaderos.invernaderosmongo.model.Catalog;
import com.espe.edu.invernaderos.invernaderosmongo.model.Cycle;
import com.espe.edu.invernaderos.invernaderosmongo.model.Step;
import com.espe.edu.invernaderos.invernaderosmongo.persistence.PersistenceManager;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author invernaderos
 */
@Stateless
public class CycleService {

    private CycleDAO cycleDAO = new CycleDAO();
    PersistenceManager manager = new PersistenceManager();
    private CatalogDAO catalogDAO = new CatalogDAO();
    private StepDAO stepDAO = new StepDAO();
    
    @EJB
    CatalogService catalogService;
    @EJB
    StepService stepService;
    

    public void insert(Cycle cycle) {
        List<Catalog> catalogs=catalogService.getAll();
        cycleDAO.save(cycle);
        System.out.println("Creating steps for catalogs:" +catalogs.size());
        boolean isFirst=true;
        for (Catalog catalog : catalogs) {
            Step step=new Step();
            if (isFirst) {
                step.setStart_date(new Date().getTime());
                isFirst=false;
            }
            step.setCycle(cycle);
            step.setStepName(catalog);
            stepDAO.save(step);
        }
        if (catalogs.size()>=1) {
            cycle.setStep(catalogs.get(0).getName());
            cycleDAO.save(cycle);
        }
        
    }

    public List<Cycle> getAll() {
        return cycleDAO.find().asList();
    }

    public Cycle findLastCycle() {
        List<Cycle> cycleTmp = getAll();
        return cycleTmp.get(cycleTmp.size() - 1);
    }

    public Cycle getByName(String name) {
        List<Cycle> ciclos = getAll();
        for (Cycle cycle : ciclos) {
            if (cycle.getName().equals(name)) {
                return cycle;
            }
        }
        System.out.println("Cycle not found: " + name);
        return null;
    }
    
/*    public boolean updateCurrentStep(){
        Cycle cycle = findLastCycle();
        List<Catalog> catalogs = catalogService.getAll();
        for (int i = 0; i < catalogs.size(); i++) {
            Catalog catalog = catalogs.get(i);
            if (catalog.getName().equals(cycle.getName())) {
                if () {
                    
                }
            }
            
        }
    }*/
    
    public void updateStep(String name){
        Cycle cycle = findLastCycle();
        Catalog catalog=null;
        List<Catalog> catalogs = catalogService.getAll();
        List<Step> steps = stepService.findAll();
        
        for (Catalog c : catalogs) {
            if (c.getName().equals(name)) {
                catalog = c;
                break;
            }
        }
        if (catalog!=null) {
            cycle.setStep(name);
            cycleDAO.save(cycle);
        }else{
            System.out.println("No encontrado.");
        }
        
            
        
    }

    public void updateState() {
        List<Cycle> cicle = getAll();
        Catalog catalogtmp = this.manager.context().find(Catalog.class).field("name").equal(cicle.get(cicle.size() - 1).getStep()).get();
        Catalog catalogUpdate = this.manager.context().find(Catalog.class).field("identifier").equal(catalogtmp.getIdentifier() + 1).get();
        Datastore ds = manager.context();
        UpdateOperations<Cycle> ops = ds.createUpdateOperations(Cycle.class).
                set("step", catalogUpdate.getName());
        Query<Cycle> updaQuery = ds.createQuery(Cycle.class).field("name").
                equal(cicle.get(cicle.size() - 1).getName());
        ds.update(updaQuery, ops);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
