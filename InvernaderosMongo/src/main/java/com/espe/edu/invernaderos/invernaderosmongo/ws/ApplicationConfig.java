/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.ws;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Andres Vr
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.espe.edu.invernaderos.invernaderosmongo.ws.WsResource.class);
        resources.add(com.espe.edu.invernaderos.invernaderosmongo.ws.test.GenericResource.class);
    }
    
    
    
}
