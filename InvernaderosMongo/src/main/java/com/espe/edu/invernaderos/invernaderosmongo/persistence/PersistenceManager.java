/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.persistence;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoOptions;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 *
 * @author Luig Rocha
 */
public class PersistenceManager {

    private static Morphia morphia;
    private static MongoClient mongoClient;
    private final Datastore mds;

//    private static PersistenceManager managerSingleton;


    static {
        MongoClientOptions mongoOptions = MongoClientOptions.builder().socketTimeout(60000).connectTimeout(60000).build();
        
        try {
            List<MongoCredential> credentialsList = new ArrayList<MongoCredential>();
            MongoCredential cred=MongoCredential.createCredential("admin", "admin", "Espe.2016".toCharArray());
            credentialsList.add(cred);
         //   mongoClient = new MongoClient(new ServerAddress("192.168.100.7"), mongoOptions);
            mongoClient = new MongoClient(new ServerAddress("127.0.0.1"), credentialsList);
        
        } catch (Exception e) {
            throw new RuntimeException("Error mongodb", e);
        }
        mongoClient.setWriteConcern(WriteConcern.SAFE);
        
        morphia = new Morphia();
        morphia.mapPackage("espe.distribuidas.mongo.mongopersistenceexample.modelo", true);
        
    }

    public PersistenceManager() {
        mds = morphia.createDatastore(mongoClient, "invernadero");
        mds.ensureIndexes();
    }

    public Datastore context() {
        return this.mds;
    }

    public static Morphia getMorphia() {
        return morphia;
    }

    public static void setMorphia(Morphia morphia) {
        PersistenceManager.morphia = morphia;
    }

    public static MongoClient getMongoClient() {
        return mongoClient;
    }

    public static void setMongoClient(MongoClient mongoClient) {
        PersistenceManager.mongoClient = mongoClient;
    }

//    public static PersistenceManager getManagerSingleton() {
//         if (managerSingleton == null) {
//            managerSingleton=new PersistenceManager();
//        }
//        return managerSingleton;
//    }
//
//    public static void setManagerSingleton(PersistenceManager managerSingleton) {
//        PersistenceManager.managerSingleton = managerSingleton;
//    }


}
