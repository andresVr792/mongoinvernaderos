/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.service;

import com.espe.edu.invernaderos.invernaderosmongo.dao.CycleDAO;
import com.espe.edu.invernaderos.invernaderosmongo.dao.NodeDataDAO;
import com.espe.edu.invernaderos.invernaderosmongo.model.Cycle;
import com.espe.edu.invernaderos.invernaderosmongo.model.NodeData;
import com.espe.edu.invernaderos.invernaderosmongo.model.Step;
import com.espe.edu.invernaderos.invernaderosmongo.persistence.PersistenceManager;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Andres Vr
 */
@Stateless
public class NodeDataService {

    private NodeDataDAO nodeDataDAO = new NodeDataDAO();
    private CycleDAO cycleDAO = new CycleDAO();

    @EJB
    CycleService cycleService;
    @EJB
    StepService stepService;

    PersistenceManager manager = new PersistenceManager();

    public List<NodeData> findAll() {

        return this.nodeDataDAO.find().asList();
    }

    public void insert(NodeData nodeData) {
        Step currentStep=nowStep();
        System.out.println("CurrentStep: "+currentStep);
        nodeData.setStep(currentStep);
        this.nodeDataDAO.save(nodeData);
    }

    public Step nowStep() {

        Cycle cycle = cycleService.findLastCycle();

        //List<Cycle> cycles=cycleService.getAll();
        List<Step> steps = stepService.findAll();
        for (Step step : steps) {
            boolean isCycle=(step.getCycle().getName().equals(cycle.getName()));
            boolean isStepName=(step.getStepName().getName().equals(cycle.getStep()));
            System.out.println("isCycle: "+isCycle);
            System.out.println("isStepName: "+isStepName);
            if (isCycle && isStepName) {
                return step;
            }
        }
        return null;

    }
}
