/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.service;

import com.espe.edu.invernaderos.invernaderosmongo.dao.CatalogDAO;
import com.espe.edu.invernaderos.invernaderosmongo.model.Catalog;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Andres Vr
 */
@Stateless
public class CatalogService {

    private CatalogDAO catalogDAO = new CatalogDAO();

    public void insertCatalog(Catalog catalog) {
        System.out.println("catalog: " + catalog);
        this.catalogDAO.save(catalog);

    }

    public List<Catalog> getAll() {
        return catalogDAO.find().asList();
    }

}
