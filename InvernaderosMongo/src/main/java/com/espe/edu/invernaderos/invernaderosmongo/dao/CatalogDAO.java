/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.dao;

import com.espe.edu.invernaderos.invernaderosmongo.model.Catalog;
import com.espe.edu.invernaderos.invernaderosmongo.persistence.PersistenceManager;
import com.mongodb.MongoClient;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

/**
 *
 * @author Andres Vr
 */
public class CatalogDAO extends BasicDAO<Catalog, ObjectId>{
    
    public CatalogDAO() {
        super(Catalog.class, PersistenceManager.getMongoClient(), PersistenceManager.getMorphia(), "invernadero");
    }
    
}
