/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.service;

import com.espe.edu.invernaderos.invernaderosmongo.dao.CatalogDAO;
import com.espe.edu.invernaderos.invernaderosmongo.dao.CycleDAO;
import com.espe.edu.invernaderos.invernaderosmongo.dao.StepDAO;
import com.espe.edu.invernaderos.invernaderosmongo.model.Catalog;
import com.espe.edu.invernaderos.invernaderosmongo.model.Cycle;
import com.espe.edu.invernaderos.invernaderosmongo.model.Step;
import com.espe.edu.invernaderos.invernaderosmongo.persistence.PersistenceManager;
import com.espe.edu.invernaderos.invernaderosmongo.ws.InsertParm;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Andres Vr
 */
@Stateless
public class StepService {

    public class UpdateEndValueDate {

        private Double endValue;
        private Double endDate;
        private String name;
        private String step;

        public Double getEndValue() {
            return endValue;
        }

        public void setEndValue(Double endValue) {
            this.endValue = endValue;
        }

        public Double getEndDate() {
            return endDate;
        }

        public void setEndDate(Double endDate) {
            this.endDate = endDate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

    }

    public class UpdateStartValueDate {

        private Double startValue;
        private Double startDate;
        private String name;
        private String step;

        public Double getStartValue() {
            return startValue;
        }

        public void setStartValue(Double startValue) {
            this.startValue = startValue;
        }

        public Double getStartDate() {
            return startDate;
        }

        public void setStartDate(Double startDate) {
            this.startDate = startDate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStep() {
            return step;
        }

        public void setStep(String step) {
            this.step = step;
        }

    }
    private StepDAO stepDAO = new StepDAO();
    private CycleDAO clyCycleDAO = new CycleDAO();
    private CatalogDAO catalogDAO = new CatalogDAO();
    PersistenceManager manager = new PersistenceManager();
    @EJB
    CycleService cycleService;

    public List<Step> findAll() {
        return this.stepDAO.find().asList();
    }

    public void insert(InsertParm inParm) {
        Catalog catalogtmp = searchCatalog(inParm.getCatalogIdentity());
        Cycle cycle = cycleService.getByName(inParm.getCycleName());
        inParm.getStep().setStepName(catalogtmp);
        inParm.getStep().setCycle(cycle);
        this.stepDAO.save(inParm.getStep());
    }

    public Catalog searchCatalog(Integer identiryParam) {
        return this.manager.context().find(Catalog.class).field("identifier").equal(identiryParam).get();
    }

    public void updateEndValueDate(UpdateEndValueDate uEndValueDate) {
        Datastore datastore = manager.context();
        UpdateOperations<Step> ops = datastore.createUpdateOperations(Step.class).
                set("end_value", uEndValueDate.getEndValue()).
                set("end_date", uEndValueDate.getEndDate());
        Query<Step> updateQuery = datastore.createQuery(Step.class).field("cycle.name").equal(uEndValueDate.getName()).field("cycle.step").equal(uEndValueDate.getStep());
        datastore.update(updateQuery, ops);
    }

    public void updateStartValueDate(UpdateStartValueDate updateStartValueDate) {
        Datastore datastore = manager.context();
        UpdateOperations<Step> ops = datastore.createUpdateOperations(Step.class).
                set("start_value", updateStartValueDate.getStartValue()).
                set("start_date", updateStartValueDate.getStartDate());
        Query<Step> updateQuery = datastore.createQuery(Step.class).field("cycle.name").equal(updateStartValueDate.getName()).field("cycle.step").equal(updateStartValueDate.getStep());
        datastore.update(updateQuery, ops);
    }
}
