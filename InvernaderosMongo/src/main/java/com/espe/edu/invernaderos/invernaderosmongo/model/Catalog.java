/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.model;

import com.espe.edu.invernaderos.invernaderosmongo.persistence.BaseEntity;
import org.mongodb.morphia.annotations.Entity;

/**
 *
 * @author Andres Vr
 */
@Entity(value = "Catalog")
public class Catalog extends BaseEntity {
    private Integer identifier;
    private String name;

    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Catalog{" + "identifier=" + identifier + ", name=" + name + '}';
    }

    
}
