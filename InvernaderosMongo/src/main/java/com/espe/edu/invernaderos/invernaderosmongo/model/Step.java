/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.model;

import com.espe.edu.invernaderos.invernaderosmongo.persistence.BaseEntity;
import java.security.Timestamp;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;

/**
 *
 * @author Andres Vr
 */
@Entity(value = "Step")
public class Step extends BaseEntity {

    @Reference
    private Catalog stepName;
    private long start_date;
    private long end_date;

    @Reference
    private Cycle cycle;

   

    public Catalog getStepName() {
        return stepName;
    }

    public void setStepName(Catalog stepName) {
        this.stepName = stepName;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    @Override
    public String toString() {
        return "Step{" + "stepName=" + stepName + ", start_date=" + start_date + ", end_date=" + end_date + ", cycle=" + cycle + '}';
    }

    


}
