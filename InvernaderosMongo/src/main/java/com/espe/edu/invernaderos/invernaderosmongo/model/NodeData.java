/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.model;

import com.espe.edu.invernaderos.invernaderosmongo.persistence.BaseEntity;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;

/**
 *
 * @author Andres Vr
 */
@Entity(value = "NodeData")
public class NodeData extends BaseEntity{

    private String node;
    private Long start_time;
    private Long end_time;
    private Double relative_humidity;
    private Double luminosity;
    private Double environment_humidity;
    private Double temperature;
    private Integer recurrency;
    private Integer periocity;
    
    @Reference
    private Step step;
    
  

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    

    public Double getLuminosity() {
        return luminosity;
    }

    public void setLuminosity(Double luminosity) {
        this.luminosity = luminosity;
    }

   

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Integer getRecurrency() {
        return recurrency;
    }

    public void setRecurrency(Integer recurrency) {
        this.recurrency = recurrency;
    }

    public Step getStep() {
        return step;
    }

    public void setStep(Step step) {
        this.step = step;
    }



    public Integer getPeriocity() {
        return periocity;
    }

    public void setPeriocity(Integer periocity) {
        this.periocity = periocity;
    }

    public Double getRelative_humidity() {
        return relative_humidity;
    }

    public void setRelative_humidity(Double relative_humidity) {
        this.relative_humidity = relative_humidity;
    }

    public Double getEnvironment_humidity() {
        return environment_humidity;
    }

    public void setEnvironment_humidity(Double environment_humidity) {
        this.environment_humidity = environment_humidity;
    }

    @Override
    public String toString() {
        return "NodeData{" + "node=" + node + ", start_time=" + start_time + ", end_time=" + end_time + ", relative_humidity=" + relative_humidity + ", luminosity=" + luminosity + ", environment_humidity=" + environment_humidity + ", temperature=" + temperature + ", recurrency=" + recurrency + ", periocity=" + periocity + ", step=" + step + '}';
    }

 
}
