/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espe.edu.invernaderos.invernaderosmongo.model;

import com.espe.edu.invernaderos.invernaderosmongo.persistence.BaseEntity;
import org.mongodb.morphia.annotations.Entity;

/**
 *
 * @author Andres Vr
 */
@Entity(value = "Cycle")
public class Cycle extends BaseEntity{

  private String name;
  
  private String step;
  private Double start_value;
  private Double end_value;

 

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public Double getStart_value() {
        return start_value;
    }

    public void setStart_value(Double start_value) {
        this.start_value = start_value;
    }

    public Double getEnd_value() {
        return end_value;
    }

    public void setEnd_value(Double end_value) {
        this.end_value = end_value;
    }

    @Override
    public String toString() {
        return "Cycle{" + "name=" + name + ", step=" + step + ", start_value=" + start_value + ", end_value=" + end_value + '}';
    }

       
  
}
